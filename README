   _        ___  ____ ____  ____
  |_|_ _   / _ \/ ___/ ___||  _ \   _____  __
  _|_||_| | | | \___ \___ \| |_) | / _ \ \/ /
 |_||_|_| | |_| |___) |__) |  __/ |  __/>  <
  |_|_|_|  \___/|____/____/|_|     \___/_/\_\

  OSSP ex - Exception Handling
  Version 1.0.6 (12-Oct-2007)

  ABSTRACT

  OSSP ex is a small ISO-C++ style exception handling library for use in
  the ISO-C language. It allows you to use the paradigm of throwing and
  catching exceptions in order to reduce the amount of error handling
  code without making your program less robust.

  This is achieved by directly transferring exceptional return codes
  (and the program control flow) from the location where the exception
  is raised (throw point) to the location where it is handled (catch
  point) -- usually from a deeply nested sub-routine to a parent
  routine. All intermediate routines no longer have to make sure that
  the exceptional return codes from sub-routines are correctly passed
  back to the parent.

  The OSSP ex facility also provides advanced exception handling
  features like shielded and deferred exceptions. Additionally, OSSP ex
  allows you to choose the used underlying machine context switching
  facility and optionally support multi-threading environments by
  allowing you to store the exception catching stack in a thread-safe
  way.

  COPYRIGHT AND LICENSE

  Copyright (c) 2002-2007 Ralf S. Engelschall <rse@engelschall.com>
  Copyright (c) 2002-2007 The OSSP Project <http://www.ossp.org/>

  This file is part of OSSP ex, an exception handling library which
  can be found at http://www.ossp.org/pkg/lib/ex/.

  Permission to use, copy, modify, and distribute this software for
  any purpose with or without fee is hereby granted, provided that
  the above copyright notice and this permission notice appear in all
  copies.

  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.

  HOME AND DOCUMENTATION

  The documentation and latest release can be found on

  o http://www.ossp.org/pkg/lib/ex/
  o  ftp://ftp.ossp.org/pkg/lib/ex/

